// Tyler A. Hugo
// CSE2 hw04
// 2/17/18
// This program give the user the option to roll a dice 5 times or enter a 5 digit number. 
  // Either the roll or the digits entered are used to play the game Yahtzee. 
  // The totals and results of the game are printed for the user to see.

import java.util.Scanner; // import scanner

public class Yahtzee { // start class
 public static void main(String[] args) { // start main method
   
Scanner myScanner = new Scanner ( System.in ) ; // declare instance of scanner

int inputRoll;  // declare inputRoll to be an integer
int roll01 = 0; // initialize roll01
int roll02 = 0; // initialize roll02
int roll03 = 0; // initialize roll03
int roll04 = 0; // initialize roll04
int roll05 = 0; // initialize roll05
   
System.out.println("To type 5 digit number, enter 1. To roll random, enter 2: "); // prompt user to pick random or enter 
int usersChoice= myScanner.nextInt() ; // declare usersChoice to be an int and assign value of next int typed
   switch(usersChoice) { // start switch usersChoice
     case 1:
       System.out.println("Enter 5 digit number instead of rolling: "); // prompt user to type number instead of roll
         inputRoll = myScanner.nextInt() ; // take next int typed to use as roll 
         roll01 = (int) (inputRoll / 10000); // returns first number
         roll02 = (int) ((inputRoll % 10000) / 1000); // returns second digit
         roll03 = (int) ((inputRoll % 1000) / 100); // returns third digit
         roll04 = (int) ((inputRoll % 100) / 10); // returns fourth digit 
         roll05 = (int) ((inputRoll % 10)); // returns fourth digit
         System.out.println("Rolls: " + roll01 + "," + roll02 + "," + roll03 + "," + roll04 + "," + roll05); // print rolls
       break;
     case 2:
        roll01= (int) (Math.random() * 6 + 1); // generate random numbers 1 to 6 roll01
        roll02= (int) (Math.random() * 6 + 1); // generate random numbers 1 to 6 roll02
        roll03= (int) (Math.random() * 6 + 1); // generate random numbers 1 to 6 roll03
        roll04= (int) (Math.random() * 6 + 1); // generate random numbers 1 to 6 roll04
        roll05= (int) (Math.random() * 6 + 1); // generate random numbers 1 to 6 roll05   
        System.out.println("Rolls: " + roll01 + "," + roll02 + "," + roll03 + "," + roll04 + "," + roll05); // print rolls
       break;
   } // end switch usersChoice
  
 
// below, initialize the six components of upper section
int aces=0; int twos=0; int threes=0; int fours=0; int fives=0; int sixes=0; 
   
 if(roll01 > 6 || roll01 < 1 || roll02 > 6 || roll02 < 1 || roll03 > 6 || roll03 < 1 ||
    roll04 > 6 || roll04 < 1 || roll05 > 6 || roll05 < 1) { // start if roll is not valid
         System.out.println("Error: one or more of the numbers rolled are invalid"); // tell user there is an error
} // end if roll not valid 
  
   else{ // start else roles are valid 
      
switch(roll01) { // start switch roll01. Assign values to aces,twos,threes,fours,fives,sixes
  case 1:
    aces = aces + roll01;
    break;
  case 2:
    twos = twos + roll01;
    break;
  case 3:
    threes = threes + roll01;
    break;
  case 4:
    fours = fours + roll01;
    break;
  case 5:
    fives = fives + roll01;
    break;
  case 6:
    sixes = sixes + roll01;
    break;
} // end switch 01

switch(roll02) { // start switch roll02. Assign values to aces,twos,threes,fours,fives,sixes
  case 1:
    aces = aces + roll02;
    break;
  case 2:
    twos = twos + roll02;
    break;
  case 3:
    threes = threes + roll02;
    break;
  case 4:
    fours = fours + roll02;
    break;
  case 5:
    fives = fives + roll02;
    break;
  case 6:
    sixes = sixes + roll02;
    break;
} // end switch roll02
   
switch(roll03) { // start switch roll03. Assign values to aces,twos,threes,fours,fives,sixes
  case 1:
    aces = aces + roll03;
    break;
  case 2:
    twos = twos + roll03;
    break;
  case 3:
    threes = threes + roll03;
    break;
  case 4:
    fours = fours + roll03;
    break;
  case 5:
    fives = fives + roll03;
    break;
  case 6:
    sixes = sixes + roll03;
    break;
} // end switch roll03
   
switch(roll04) { // start switch roll04. Assign values to aces,twos,threes,fours,fives,sixes
  case 1:
    aces = aces + roll04;
    break;
  case 2:
    twos = twos + roll04;
    break;
  case 3:
    threes = threes + roll04;
    break;
  case 4:
    fours = fours + roll04;
    break;
  case 5:
    fives = fives + roll04;
    break;
  case 6:
    sixes = sixes + roll04;
    break;
} // end switch roll04  
   
switch(roll05) { // start switch roll05. Assign values to aces,twos,threes,fours,fives,sixes
  case 1:
    aces = aces + roll05;
    break;
  case 2:
    twos = twos + roll05;
    break;
  case 3:
    threes = threes + roll05;
    break;
  case 4:
    fours = fours + roll05;
    break;
  case 5:
    fives = fives + roll05;
    break;
  case 6:
    sixes = sixes + roll05;
    break;
} // end switch roll05
  // below, print results for aces,twos,threes,fours,fives,sixes   
   System.out.println("Aces: " + aces); // print aces
   System.out.println("Twos: " + twos); // print twos
   System.out.println("Threes: " + threes); // print threes
   System.out.println("Fours: " + fours); // print fours
   System.out.println("Fives: " + fives); // print fives
   System.out.println("Sixes: " + sixes); // print sixes
     
   int totalUpper = aces + twos + threes + fours + fives + sixes; // assign value to totalUpper
   System.out.println("TOTAL Upper Section: " + totalUpper); // print totalUpper

int bonus=0; // initialize bonus
int totalBonus=0; // initialize total with bonus     
if(totalUpper >= 63) { // start if bonus
  bonus = 35; // add 35 point bonus if total is 63 or greater
} // end if bonus

  totalBonus= totalUpper + bonus; // assign value to total including bonus
     
  System.out.println("Bonus: " + bonus); // print bonus
  System.out.println("TOTAL + Bonus: " + totalBonus); // print total including bonus
    
int threeOfAKind = 0; // initialize threeOfAKind
int fourOfAKind = 0; // initialize fourOfAKind
int yahtzee = 0; // initialize yahtzee
    
switch(aces) { // start switch aces
  case 3:
    threeOfAKind = 3;
    break;
  case 4:
    fourOfAKind = 4;
    break;
  case 5:
    yahtzee = 50;
    break;
} // end switch aces
 
switch(twos) { // start switch twos
  case 6:
    threeOfAKind = 6;
    break;
  case 8:
    fourOfAKind = 8;
    break;
  case 10:
    yahtzee = 50;
    break;
} // end switch twos
  
switch(threes) { // start switch threes
  case 9:
    threeOfAKind = 9;
    break;
  case 12:
    fourOfAKind = 12;
    break;
  case 15:
    yahtzee = 50;
    break;
} // end switch threes
    
switch(fours) { // start switch fours
  case 12:
    threeOfAKind = 12;
    break;
  case 16:
    fourOfAKind = 16;
    break;
  case 20:
    yahtzee = 50;
    break;
} // end switch fours
  
switch(fives) { // start switch fives
  case 15:
    threeOfAKind = 15;
    break;
  case 20:
    fourOfAKind = 20;
    break;
  case 25:
    yahtzee = 50;
    break;
} // end switch fives
    
switch(sixes) { // start switch sixes
  case 18:
    threeOfAKind = 18;
    break;
  case 24:
    fourOfAKind = 24;
    break;
  case 30:
    yahtzee = 50;
    break;
} // end switch sixes

int smStraight=0; int lgStraight=0; // initialize smStraight and lgStraight
    
if( roll01==1 && roll02==2 && roll03==3 && roll04==4 && roll05==5 ) { // start if straight
  lgStraight=40;
} // end straight
else if( roll01==2 && roll02==3 && roll03==4 && roll04==5 && roll05==6 ) { // start else if straight 1
    lgStraight=40;
  } // end else if straight 1 
else if( roll01==1 && roll02==2 && roll03==3 && roll04==4 ) { // start else if straight 2
      smStraight=30;
    } // end else if straight 2 
else if( roll01==2 && roll02==3 && roll03==4 && roll04==5 ) { // start else if straight 3
     smStraight=30;
} // end if else straight 3
else if( roll01==3 && roll02==4 && roll03==5 && roll04==6 ) { // start else if straight 4
     smStraight=30;
} // end if else straight 4
else if( roll02==1 && roll03==2 && roll04==3 && roll05==4 ) { // start else if straight 5
     smStraight=30;
} // end if else straight 5
else if( roll02==2 && roll03==3 && roll04==4 && roll05==5 ) { // start else if straight 6
     smStraight=30;
} // end if else straight 6
else if( roll02==3 && roll03==4 && roll04==5 && roll05==6 ) { // start else if straight 7
     smStraight=30;
} // end if else straight 7
    
int chance= roll01 + roll02 + roll03 + roll04 + roll05; // declare chance to be an int and assign it to be the sum of the rolls
      
int fullHouse=0;

if( (aces==3 || twos==6 || threes==9 || fours==12 || fives==15 || sixes==18) &&
  (aces==2 || twos==4 || threes==6 || fours==8 || fives==10 || sixes==12) ) { // start if full house
  fullHouse=25;
} // end if full house
 
    System.out.println("Three of a kind: " + threeOfAKind); // print three of a kind
    System.out.println("Four of a kind: " + fourOfAKind); // print four of a kind
    System.out.println("Full house: " + fullHouse); // print full house 
    System.out.println("Small Straight: " + smStraight); // print small straight 
    System.out.println("Large Straight: " + lgStraight); // print large straight
    System.out.println("Yahtzee: " + yahtzee); // pring yahtzee
    System.out.println("Chance: " + chance);
  int totalLower= (threeOfAKind + fourOfAKind + fullHouse + smStraight + lgStraight + yahtzee + chance); // total lower half
    System.out.println("TOTAL of lower half: " + totalLower); // print total lower  
    System.out.println("TOTAL of upper half: " + totalBonus); // print total upper half including bonus
  int grandTotal= (totalBonus + totalLower); // declare and assign value to grand total
    System.out.println("GRAND TOTAL: " + grandTotal); // pring grand total
     
     
} // end else roles are valid

 
 } // end main method
} // end class