// Tyler Hugo
// CSE2 Hw07
// 3/27/18
// This program 

import java.util.Random; // import random 
import java.util.Scanner; // import scanner

public class Methods { // start class
  public static void main(String[] args) { // start main method
  
Scanner myScanner = new Scanner(System.in);
 int anotherSen;
do{ // start do      
System.out.println("The "+adjective(0)+" "+adjective(0)+" "+nounOne(0)+" "+verbGen(0)+" the "+adjective(0)+" "+nounTwo(0)+"." );
System.out.println();
System.out.println("If you would like another sentence, enter '1' : ");
   anotherSen = myScanner.nextInt();
}while(anotherSen == 1);
    
  } // end main method
 
public static String adjective(int random) { // start method adj
  Random randomGenerator = new Random(); // create random object
    random = randomGenerator.nextInt(10); // generates random int <10
    
  String tempAdj = "*"; 
  switch (random) {
    case 0:
      tempAdj = "gentle";
      break;
    case 1:
      tempAdj = "big";
      break;
    case 2: 
      tempAdj = "fun";
      break;
    case 3:
      tempAdj = "fast";
      break;
    case 4: 
      tempAdj = "slow";
      break;
    case 5:
      tempAdj = "small";
      break;
    case 6:
      tempAdj = "happy";
      break;
    case 7:
      tempAdj = "sad";
      break;
    case 8:
      tempAdj = "funny";
      break;
    case 9: 
      tempAdj = "grumpy";
      break;
  }
  
  return tempAdj;  
} // end method adj 

public static String nounOne(int random) { // start method noun1
  Random randomGenerator = new Random(); // create random object
  random = randomGenerator.nextInt(10); // generates random int <10
  
  String temp = "*"; 
  switch (random) {
    case 0:
      temp = "pig";
      break;
    case 1:
      temp = "dog";
      break;
    case 2: 
      temp = "cat";
      break;
    case 3:
      temp = "cow";
      break;
    case 4: 
      temp = "boy";
      break;
    case 5:
      temp = "girl";
      break;
    case 6:
      temp = "fox";
      break;
    case 7:
      temp = "turtle";
      break;
    case 8:
      temp = "fish";
      break;
    case 9: 
      temp = "bear";
      break;
  }
  return temp;  
} // end method noun1 

public static String verbGen(int random) { // start method verb
  Random randomGenerator = new Random(); // create random object
  random = randomGenerator.nextInt(10); // generates random int <10
  
  String temp = "*"; 
  switch (random) {
    case 0:
      temp = "saw";
      break;
    case 1:
      temp = "ran by";
      break;
    case 2: 
      temp = "fell on";
      break;
    case 3:
      temp= "jumped over";
      break;
    case 4: 
      temp = "swam by";
      break;
    case 5:
      temp = "walked by";
      break;
    case 6:
      temp = "passed";
      break;
    case 7:
      temp = "hopped over";
      break;
    case 8:
      temp = "looked at";
      break;
    case 9: 
      temp = "moved";
      break;
  }
  return temp;  
} // end method verb
  
public static String nounTwo(int random) { // start method noun2
  Random randomGenerator = new Random(); // create random object
  random = randomGenerator.nextInt(10); // generates random int <10
  
  String temp = "*"; 
  switch (random) {
    case 0:
      temp = "tiger";
      break;
    case 1:
      temp = "frog";
      break;
    case 2: 
      temp = "mouse";
      break;
    case 3:
      temp= "snake";
      break;
    case 4: 
      temp = "man";
      break;
    case 5:
      temp = "woman";
      break;
    case 6:
      temp = "teacher";
      break;
    case 7:
      temp = "squirrel";
      break;
    case 8:
      temp = "deer";
      break;
    case 9: 
      temp = "worm";
      break;
  }
  return temp;  
} // end method noun2
} // end class 
    