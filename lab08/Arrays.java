// Tyler Hugo
// CSE2 lab08
// 4/6/18
// this program generates an int 5-10 and has user input names for correct number of students
// the names are stored in the array "students". a grade of 0-100 is randomly generated for each student
// the grades are stored in the array "midterm". the students names and corresponding grades are printed 
import java.util.Scanner;
import java.util.Random; 
public class Arrays{ 
public static void main(String[] args){
 
Random randomNum = new Random(); // random number generator 
Scanner myScanner = new Scanner(System.in); // initialize scanner 
int numOfStudents; // initialize numOfStudents 
// below, generates random number 5-10 
do {  
  numOfStudents = randomNum.nextInt(11);
} while (numOfStudents < 5);
  
String[] students = new String [numOfStudents]; // create array for student names
  
int[] midterm = new int [numOfStudents]; // create array for student grades 
  
System.out.println("Enter " + numOfStudents + " student names: "); // have user enter the decided number of student names

  for(int j=0; j < numOfStudents; j++){   
    String tempName = myScanner.next();
    students[j] = tempName; // assign name to array member
    System.out.println(tempName); // print each name 
  } // end for j 
System.out.println("Here are the midterm grades of the " + numOfStudents + " students above: ");
  
  for(int i=0; i < numOfStudents; i++){   
    int tempGrade = randomNum.nextInt(101); // generate random number 0-100
     midterm[i] = tempGrade; // assign number to array member 
     System.out.println(students[i] + " : " + midterm[i]); // print student name with their grade 
      
  } // end for i 
  

} // end main method 
} // end class