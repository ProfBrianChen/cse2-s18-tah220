// Tyler Hugo
// CSE2 Hw07
// 3/27/18
// This program allows user to chose between rectangle, triangle, and circle
// then the program uses multiple methods to test if the input is the correct type
// and calculate the shape's area.

import java.util.Scanner; // import scanner

public class Area { // start class
  public static void main(String[] args) { // start main method 
    
Scanner myScanner = new Scanner(System.in); // initialize scanner
String shape; // initialize shape 
System.out.println("Enter shape type: "); // prompt user to enter shape type
   shape = myScanner.next(); // assign input to = shape 
   // below: test that correct shape was entered, and if not, ask again 
   while (!shape.equals("rectangle") && !shape.equals("triangle") && !shape.equals("circle")) { // start while 
     System.out.println("Enter shape type 'rectangle', 'triangle', or 'circle': ");
     shape = myScanner.next();
   } // end while 
   System.out.println("Shape: " + shape); // print shape 
  
 // below if shape is rectangle, calculate area
 if (shape.equals("rectangle")) { // if rect
   System.out.println("Enter rectangle's 'baselength' then 'height': ");
   double length = 0;
   length = testDouble(length); // test if input is a double
   double heightR = 0;
   heightR = testDouble(heightR); // test if input is a double
   System.out.println("Length: " + length + " Height: " +  heightR);
   double areaR = areaRectangle(length, heightR); // calculate area 
   System.out.println("Area of Rectangle: " + areaR);
 } // if rec 
 
 // below if shape is triangle, calculare area
 if (shape.equals("triangle")) {
   System.out.println("Enter triangle's 'baselength' then 'height': ");
   double base = 0;
   base = testDouble(base); // test if double 
   double heightT = 0;
   heightT = testDouble(heightT); // test if double 
   System.out.println("Base length: " + base + " Height: " + heightT); 
   double areaT = areaTriangle(base, heightT); // calculate area 
   System.out.println("Area of Triangle: " + areaT);
 }

 // below if shape is circle, calculate area using 3.14 as PI 
 if (shape.equals("circle")) {
   System.out.println("Enter circle's 'radius': ");
   double radius = 0;
   radius = testDouble(radius); // test if double 
   System.out.println("Radius: " + radius);
   double areaC = areaCircle(radius); // calculate area 
   System.out.println("Area of Circle: " + areaC);
 }
 } // end main method 

// below is the method to test if input is a double 
public static double testDouble(double input) { // start method double
Scanner myScanner = new Scanner(System.in); // initialize scanner
  System.out.println("Enter dimension one as a double: "); 
  while(!myScanner.hasNextDouble() || myScanner.hasNextInt()) { 
   String junkWord = myScanner.next(); // clears scanner 
   System.out.println("Error: make sure dimensions are doubles: ");   
}
  input = myScanner.nextDouble(); 
  return input;
} // end method double 

// below is the method to calculate rectangle area 
public static double areaRectangle(double l, double h) { // start method rect area
  return(l * h);
} // end method rect area 

// below is the method to calculate triangle are a 
public static double areaTriangle(double b, double h) { // start method tri area
  return(b / 2 * h);
} // end method tri area 
 
// below is the method to calculate circle area with 3.14 for PI
public static double areaCircle(double r) {
  return(Math.pow(r,2.0) * 3.14);
}  
} // end class



 