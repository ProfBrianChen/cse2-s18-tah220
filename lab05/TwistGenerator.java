// Tyler A. Hugo
// CSE2 Lab05 3/2/2018
// This program does...

import java.util.Scanner; // import scanner class

public class TwistGenerator { // start class
  public static void main(String[] args) { // start main method
 
int length = -1; // initialize int variable length 
Scanner myScanner = new Scanner(System.in); // initialize scanner
 
while (length <= 0) { // start while positive
  System.out.println("Enter a positive integer: "); // prompt user to enter length  
  while (!myScanner.hasNextInt()) { // start while loop length is an int
    String junkWord = myScanner.next(); // clear out scanner
    System.out.println("Enter a positive integer again: "); // prompt user to enter length again
  } // end while loop length is an int  
  length = myScanner.nextInt(); 
  System.out.println("Length: " + length); // print length 
} // end while positive 

    System.out.println("LengthEnd: " + length); // print length 
    
for (int i = 0; i < length; i ++) { // start for twist line 1
  int toggle = i % 3;
  switch(toggle) { // start switch 
    case 0:
      System.out.print("\\");
      break;
    case 1:
      System.out.print(" ");
      break;
    case 2: 
      System.out.print("/");
      break; 
  } // end switch
} // end for twist line 1  
    System.out.println(); 
    
for (int i = 0; i < length; i ++) { // start for twist line 2
  int toggle = i % 3;
  switch(toggle) { // start switch 
    case 1:
      System.out.print("X");
      break;
    case 0:
    case 2: 
      System.out.print(" ");
      break; 
  } // end switch
} // end for twist line 2  
    System.out.println();
    
for (int i = 0; i < length; i ++) { // start for twist line 3
  int toggle = i % 3;
  switch(toggle) { // start switch 
    case 0:
      System.out.print("/");
      break;
    case 1:
      System.out.print(" ");
      break;
    case 2: 
      System.out.print("\\");
      break; 
  } // end switch
} // end for twist line 3  
    System.out.println();
    
  } // end main method 
  
} // end class