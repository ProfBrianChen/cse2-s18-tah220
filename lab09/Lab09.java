// Tyler Hugo
// 4/15/18

public class Lab09 {
  public static void main(String[] args) {
   
int[] array0 = new int[]{1,2,3,4,5,6,7,8};
int[] array1 = new int[8];
int[] array2 = new int[8];
   
array1 = copy(array0);
array2 = copy(array0);
    
inverter(array0); // pass array0 to inverter 
	System.out.println("Inverted array0: ");
    print(array0); // print array 0 
		
System.out.println("Inverted2 array1: ");  
 inverter2(array1);
 print(array1); // pass array 1 to inverter2 and print with print()
		
System.out.println("Inverted2 array3: ");      
int[] array3 = inverter2(array2); // assign inverted array2 to array 3 
    print(array3); // print array 3
 
    
  } // end main Method 

public static int[] copy(int[] list) { 
  int[] newCopy = new int[list.length];
  for(int i = 0; i < list.length; i++) {
    newCopy[i] = list[i];
  } // end for 
  return newCopy; 
} // end method copy 
  
public static void inverter(int[] list) { 
	for(int i = 0; i < list.length / 2; i++) {
		 int temp = list[i];
		 list[i] = list[list.length - 1 -i];
     list[list.length - 1 -i] = temp;
   } // end for
	} // end method inverter 
 
public static int[] inverter2(int[] list) {
  int[] copyList = new int[list.length]; 
  copyList = copy(list);
	for(int i = 0; i < copyList.length / 2; i++) {
		 int temp = copyList[i];
		 copyList[i] = copyList[copyList.length - 1 -i];
     copyList[copyList.length - 1 -i] = temp;
     //index --; 
  } // end for
  return copyList;
	
}  // end method inverter2
  
public static void print(int[] input) { 
  String out="{";
	for(int j=0;j<input.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=input[j];
	}
	out+="} ";
	System.out.println(out);
} // end metod print 
 
} // end class