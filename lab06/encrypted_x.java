// Tyler Hugo
// CSE2 lab06 3/9/18
// This program takes input from a user and checks if it is a positive 
// integer between 0-100. Then it prints an encrypted "X" inside a square
// with demensions the user entered. 

import java.util.Scanner; // import scanner class

public class encrypted_x { // start class
  public static void main(String[] args) { // start main method
    
int input = -1; // assign value to input     
Scanner myScanner = new Scanner(System.in); // initialize scanner

// below check if input is an integer and between 0-100 
do { // start do while  
System.out.println("Enter an integer between 0-100: "); // ask user to input size of square
if(myScanner.hasNextInt() == true) { // start if   
  input = myScanner.nextInt(); // assign input to equal the entered integer
} // end if  
else { // start else 
  while(!myScanner.hasNextInt()) { // start while 
    String junkWord = myScanner.next(); // clears scanner
    System.out.println("Error: enter input as an integer: "); // prompt user to enter input again with correct type 
       // continues to ask until an int is entered 
  } // end while 
    input = myScanner.nextInt(); // assign input to equal the entered integer
} // end else 
System.out.println("input is:" + input); // print input 
} while(input > 100 || input < 0); // end do while
    
// below for loops used to create x 

for(int i = 0; i < input; i++) { // start for how many lines
  for(int j = 0; j < input; j++){ // start for what is on each line
    if(j==i || j==(input-i)) { // start if  
      System.out.print(" "); // print space 
    } // end if
    else { // start else
      System.out.print("*"); // print *
    } // end else
  } // end for what is on each line 
  System.out.println(); // print blank line 
} // end for how many lines
 
  } // end main method
} // end class