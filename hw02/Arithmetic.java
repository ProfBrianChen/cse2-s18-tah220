// Tyler Hugo
// Feb 5, 2018
// CSE2
// This program will calculate total cost of purchases before sales tax, total sales tax on the purchases,
// and total cost of purchases after tax. 

public class Arithmetic {
  // main method
  public static void main(String[] args) {
    
// Number of pairs of pants
    int numPants = 3;
// Cost per pair of pants
    double pantsPrice = 34.98;
    
// Number of sweatshirts
    int numSweatshirts = 2;
// Cost per sweatshirt
    double shirtPrice = 24.99;
    
// Number of belts
    int numBelts = 1;
// Cost per belt
    double beltCost = 33.99;
// The tax rate
    double paSalesTax = 0.06;
    
// Total cost of each item
    double totalCostOfPants, // declare totalCostOfPants to be a double
    totalCostOfShirts, // declare totalCostOfShirts to be a double
    totalCostOfBelts; // declare totalCostOfBelts to be a double 
    
    totalCostOfPants = numPants * pantsPrice; // assign totalCostOfPants to be numPants times pantsPrice
    totalCostOfShirts = numSweatshirts * shirtPrice; // assign totalCostOfShirts to be numSweatshirts times shirtPrice
    totalCostOfBelts = numBelts * beltCost; // assign totalCostOfBelts to be numBelts times beltCost
    
// Sales tax for each item
    double salesTaxOnPants = totalCostOfPants * paSalesTax; // declare salesTaxOnPants to be a double
    double salesTaxOnShirts = totalCostOfShirts * paSalesTax; // declare salesTaxOnShirts to be a double
    double salesTaxOnBelts = totalCostOfBelts * paSalesTax; // declare salesTaxOnBelts to be a double
    
// Total cost of purchases before tax
    double totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
      // declare totalCostOfPurchases to be a double
    
// Total sales tax
    double totalSalesTax = salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts; // declare totalSalesTax to be a double
    
// Total paid for this transaction, including sales tax
    double overallTotalCost = totalCostOfPurchases + totalSalesTax; // declare overallTotalCost to be a double
    
// Print total cost of purchases before tax
    System.out.println("Total cost of purchases before tax = $"+
           (totalCostOfPurchases)); // print totalCostOfPurchases in dollars
// Print total sales tax
    System.out.println("Total sales tax =$"+
           Math.round((totalSalesTax * 100.0)) / 100.0); // print totalSalesTax tax in dollars
// Print total ammount paid for transaction including sales tax
    System.out.println("Total cost after tax = $"+
           Math.round((overallTotalCost * 100.0)) / 100.0); // print overallTotalCost in dollars 
    
    
    
  } // end of main method
} // end of class