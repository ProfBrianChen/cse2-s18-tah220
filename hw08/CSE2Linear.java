// Tyler Hugo
//
import java.util.Scanner;
import java.util.Random; 
public class CSE2Linear{ 
public static void main(String[] args){
 
Scanner myScanner = new Scanner(System.in); // initialize scanner 
System.out.println("Enter 15 integers for students' grades: ");
int tempGrade;
int[] studentGrade = new int [15]; // create array for student grades 
  
for(int j=0; j < 15; j++){ 
  if(!myScanner.hasNextInt() == true){ // test if input are integers 
    System.out.println("ERROR: a non-integer was entered.");
    break;
  } // end if integer 
  else { // start else 
     tempGrade = myScanner.nextInt(); // assign value to tempGrade 
  } // end else 
  if(tempGrade < 0 || tempGrade > 100){ // test if input is in range 0-100 
    System.out.println("ERROR: input is out of range.");
    break;
  } // end if in range 
  studentGrade[j] = tempGrade; // assign grade to array member 
 // below if statements test if int is >= last int
  if (j>0){ 
    if (studentGrade[j] < studentGrade[j-1]) {
      System.out.println("ERROR: int is not >= last int.");
      break; 
    } // end if 
  } // end if      
  System.out.println("array "+j+" : "+studentGrade[j]); // print array member with corresponding grade 
  } // end for j 
  
System.out.println("Enter a grade to search for: "); // prompt user to enter grade to search for
  int search = myScanner.nextInt(); // assign value to search 
  
 binarySearch(studentGrade, search);
 
 /** below, copy array 
 int copy[] = new int [15];
 for(int i = 0; i < 15; i++){
   copy[i] = studentGrade[i];
 }  */
 scrambleArray(studentGrade);
  
 System.out.println("Enter another grade to search for: ");
   int search2 = myScanner.nextInt(); // assign value to search2
  
 linearSearch(studentGrade, search2); 

} // end main method
  
public static void binarySearch(int[] list, int key) { // start method binary
  int low = 0;
  int high = 14;
  int i = 0; 
  while(high >= low){ 
  i ++; 
  int mid = (low + high) / 2;
   if (key < list[mid]) {
     high = mid - 1;
   } // end if < 
   else if (key == list[mid]) {
     System.out.println(key + " was found in the list with "+i+" iterations.");
     return; 
   } // end if ==
  else {
    low = mid + 1;
  } // end else 
  } // end while 
 System.out.println(key + " was not found in the list with "+i+" iterations."); 
} // end method binary
  
public static void scrambleArray(int[] list){
  Random randomNum = new Random(); // random number generator
  int index, temp; 
  for(int i = list.length - 1; i > 0; i--){ 
   index = randomNum.nextInt(i + 1);
     
   temp = list[index];
    
   list[index] = list[i];
     //System.out.println("list[tatget] : "+ list[target]);
   list[i] = temp;
     System.out.println("scram array["+i+"] : " + list[i]); 
  }
}// end method scrambleArray 
  
public static void linearSearch(int[] list, int key) { // start method linear
  int i;
  for(i = 0; i < 15; i++){
     if(key == list[i]){
       System.out.println(key + " was found in the list with "+i+" iterations.");
       return;
     } // end if finds key
  } // end for 
  System.out.println(key + " was not found in the list with "+i+" iterations.");

} // end method linear 
  
} // end class 

