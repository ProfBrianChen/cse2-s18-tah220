import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(1); //***METHOD RANDOMINPUT***
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index); // ***METHOD DELETE***
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target); // *** METHOD REMOVE *** 
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  
  } // end main method 
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  } // end method listArray 
  
 public static int[] randomInput(int a){
   Random randomNum = new Random(); // random number generator 
   int[] arrayRandom = new int[10]; // create array
   int numTemp; 
   for(int i = 0; i < arrayRandom.length; i++){
     numTemp = randomNum.nextInt(10); // generate random num 0-9 for each member of array 
     arrayRandom[i] = numTemp; // assign number to member of array 
   } //end for
   return arrayRandom; // return the random array 
 } // end method random 
  
 public static int[] delete(int[] list, int pos){
   int[] arrayDelete = new int[list.length - 1]; // create new array 
   if(pos > (list.length-1) || pos < 0){
     System.out.println("The index is not valid.");
     return list;
   } // end if not valid 
	 int index = 0;
   for(int i = 0; i < list.length; i++){
     if (i != pos) {
			 arrayDelete[index] = list[i];
			 index++;
		 } // end if 
   } // end for 
   return arrayDelete;
 } // end method delete 
	
public static int[] remove(int[] list, int target){
	int count = 0;  
	for(int i = 0; i < list.length; i++){
		 if(list[i] == target){
			 count++;
		 } // end if 
	} // end for 
	System.out.println("count: "+count);
	
	if(count == 0){
		System.out.println("Element "+target+" was not found.");
		return list;
	} // end if count =0
	
	System.out.println("Element "+target+" has been found.");
  int[] arrayRemove = new int[list.length - count];
	int index = 0;
	for(int j = 0; j < list.length; j++){
		if(list[j] != target){
			arrayRemove[index] = list[j];
       index++;
		} // end if
	}// end for j 
	return arrayRemove;
	
}// end method remove
} // end class
