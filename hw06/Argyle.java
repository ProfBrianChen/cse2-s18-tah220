// Tyler Hugo
// CSE2 hw06 3/20/18
// this program...

import java.util.Scanner; // import scanner class

public class Argyle { // start class
  public static void main(String[] args) { // start method
    
Scanner myScanner = new Scanner(System.in); // initialize scanner

// test display window width
int widthDisplay = -1; // initialize widthDisplay
while (widthDisplay <= 0) { // start while positive
  System.out.println("Enter a positive integer for display width : "); // prompt user to enter width  
  while (!myScanner.hasNextInt()) { // start while loop width is an int
    String junkWord = myScanner.next(); // clear out scanner
    System.out.println("Error: enter a positive integer for display width: "); // prompt user to enter width again
  } // end while loop width is an int  
  widthDisplay = myScanner.nextInt(); 
  System.out.println("Display window width: " + widthDisplay); // print width  
} // end while positive 

// test display window height 
int heightDisplay = -1; // initialize heightDisplay
while (heightDisplay <= 0) { // start while positive
  System.out.println("Enter a positive integer for display height : "); // prompt user to enter height  
  while (!myScanner.hasNextInt()) { // start while loop height is an int
    String junkWord = myScanner.next(); // clear out scanner
    System.out.println("Error: enter a positive integer for display height: "); // prompt user to enter height again
  } // end while loop height is an int  
  heightDisplay = myScanner.nextInt(); 
  System.out.println("Display window height: " + heightDisplay); // print height  
} // end while positive 
    
// test size of diamond
int diamondSize = -1; // initialize diamondSize
while (diamondSize <= 0) { // start while positive
  System.out.println("Enter a positive integer for diamond size : "); // prompt user to enter size  
  while (!myScanner.hasNextInt()) { // start while loop size is an int
    String junkWord = myScanner.next(); // clear out scanner
    System.out.println("Error: enter a positive integer for dismaond size: "); // prompt user to enter size again
  } // end while loop size is an int  
  diamondSize = myScanner.nextInt(); 
  System.out.println("Diamond size: " + diamondSize); // print diamond size  
} // end while positive 
    
// test width of argyle stripe
int stripeSize = -1; // initialize stripeSize
boolean isInteger;    
do { // start do students
  System.out.println("Enter a positive, odd integer for stripe size : "); // prompt user to enter width of stripe
  if (myScanner.hasNextInt()) { // start if
    stripeSize = myScanner.nextInt(); // assign value of stripeSize to equal integer entered
    isInteger = true; // is an integer
  } // end if
  else { // start else
    System.out.println("ERROR: input must be an integer");
    isInteger = false; // not an integer
    String junkWord = myScanner.next(); // clears scanner
  } // end else
} while ((!isInteger) || (stripeSize > (diamondSize / 2)) || (stripeSize % 2 != 1)) ; // test if input is acceptable 
    System.out.println("Stripe width: " + stripeSize); // print width of stripe 
    
// test diamond fill characters
char fill1, fill2, fill3; // initialize fill variables 
do { 
System.out.println("Enter diamond fill character 1: "); // prompt user to enter diamond fill 1  
String temp = myScanner.next(); // store input in temporary string variable 
  fill1 = temp.charAt(0); // convert to char 
  System.out.println("Diamond fill 1: " + fill1); // print fill 1 
    
System.out.println("Enter diamond fill character 2: "); // prompt user to enter diamond fill 2 
String temp1 = myScanner.next(); // store input in temporary string variable
  fill2 = temp.charAt(0); // convert to char 
  System.out.println("Diamond fill 2: " + fill2); // print fill 2
} while(fill1 != fill2); // make sure variables are not the same 
System.out.println("Enter stripe fill character: ");  // prompt user to enter stripe fill 
String temp2 = myScanner.next(); // store input in temporary string variable
  fill3 = temp2.charAt(0); // convert to char 
  System.out.println("Stripe fill: " + fill3); // print stripe fill 
    
for(i=0; i<=stripeSize; i++) {
  System.out.print(fill3); 
}
    
  } // end method 
  
} // end class
