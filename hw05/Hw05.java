// Tyler Hugo
// CSE2 hw05
// 3/3/2018
// This program asks the user to input information about a course they are taking.
// The program asks for the course number, department name, meetings per week, start time,
// instructor name, and number of students.
// The program tests that each input entered is the correct type. The program will 
// continue to ask for input until the correct type is entered. 

import java.util.Scanner; // import scanner class

public class Hw05 { // start class
  public static void main(String[] args) { // start main method
   
Scanner myScanner = new Scanner(System.in); // initialize scanner  

int courseNumber = 0; // initialize integer variable courseNumber 
    
System.out.println("Enter course number as an integer: "); // prompt user to enter course number
if(myScanner.hasNextInt() == true) { // start if course number  
  courseNumber = myScanner.nextInt(); // assign courseNumber to equal the entered integer
} // end if course number 
else { // start else course number
  while(!myScanner.hasNextInt()) { // start while courseNumber
    String junkWord = myScanner.next(); // clears scanner
    System.out.println("ERROR: enter course number as an integer again: "); // prompt user to enter course number again with correct type 
       // continues to ask until an int is entered 
  } // end while courseNumber
    courseNumber = myScanner.nextInt(); // assign courseNumber to equal the entered integer
} // end else course number
System.out.println("Course Number: " + courseNumber); // print the course number
    
String departmentName; // initialize string variable departmentName
    
System.out.println("Enter department name as a string: "); // prompt user to enter department name
if(myScanner.hasNext() == true) { // start if departmentName
  departmentName = myScanner.next(); // assign departmentName to equal the entered string
} // end if departmentName
else { // start else departmentName
  while(!myScanner.hasNext()) { // start while departmentName
    String junkWord = myScanner.next();
    System.out.println("ERROR: enter department name as a string again: "); // prompt user to enter department name again with correct type 
       // continue asking until a string is entered
  } // end while departmentName
    departmentName = myScanner.next(); // assign departmentName to equal the entered string
} // end else departmentName
 System.out.println("Department Name: " + departmentName); // print departmentName   
    
int timesPerWeek = -1; // initialize integer variable timesPerWeek

while (timesPerWeek <= 0) { // while timesPerWeek is positive}
System.out.println("Enter the number of times the course meets per week as an integer: "); // prompt user to enter number of times course meets
if(myScanner.hasNextInt() == true) { // start if timesPerWeek  
  timesPerWeek = myScanner.nextInt(); // assign timesPerWeek to equal the entered integer
} // end if timesPerWeek 
else { // start else timesPerWeek
  while(!myScanner.hasNextInt()) { // start while timesPerWeek
    String junkWord = myScanner.next(); // clears scanner
    System.out.println("ERROR: enter meetings per week as an integer: "); // prompt user to enter timesPerWeek again with correct type 
       // continues to ask until an int is entered 
  } // end while timesPerWeek
    timesPerWeek = myScanner.nextInt(); // assign timesPerWeek to equal the entered integer
} // end else course number
} // end while timesPerWeek is positive 
System.out.println("Meetings Per Week: " + timesPerWeek); // print the course number
    
String classTime ; // initialize String variable classTime
System.out.println("Enter what time the class meets: "); // prompt user to enter meeting time
if(myScanner.hasNext() == true) { // start if classTime  
  classTime = myScanner.next(); // assign classTime to equal the entered String
} // end if classTime 
else { // start else classTime
  while(!myScanner.hasNext()) { // start while classTime
    String junkWord = myScanner.next(); // clears scanner
    System.out.println("ERROR: enter class time as a String again: "); // prompt user to enter class time again with correct type 
       // continues to ask until an int is entered 
  } // end while classTime
    classTime = myScanner.next(); // assign classTime to equal the entered String
} // end else course number
System.out.println("Class Time: " + classTime); // print the classTime    
    
String instructorName;
System.out.println("Enter your instructor's name as a string: "); // prompt user to enter instructor's name
if(myScanner.hasNext() == true) { // start if instructorName  
  instructorName = myScanner.next(); // assign instructorName to equal the entered String
} // end if instructorName 
else { // start else instructorName
  while(!myScanner.hasNext()) { // start while instructorName
    String junkWord = myScanner.next(); // clears scanner
    System.out.println("ERROR: enter instructor's name as a String again: "); // prompt user to enter instructorName again with correct type 
       // continues to ask until an int is entered 
  } // end while instructorName
    instructorName = myScanner.next(); // assign instructorName to equal the entered String
} // end else course number
System.out.println("Instructor's Name: " + instructorName); // print the instructorName 
    
int students = -1; // initialize integer variable students 
boolean isInteger; // initialize boolean variable isInteger

do { // start do students
  System.out.println("Enter the number of students in the class: "); // prompt user to enter number of students
  if (myScanner.hasNextInt()) { // start if
    students = myScanner.nextInt(); // assign value of students to equal integer entered
    isInteger = true; // is an integer
  } // end if
  else { // start else
    System.out.println("ERROR: input must be an integer");
    isInteger = false; // not an integer
    String junkWord = myScanner.next(); // clears scanner
  } // end else
} while ((!isInteger) || (students <= 0)) ; // condition testing if positive integer
    System.out.println("Number of Students: " + students); // print number of students
    
    
  } // end main method
  
} // end class