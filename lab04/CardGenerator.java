// Tyler A. Hugo
// CSE2, lab04
// 12/16/18
// This program does...

public class CardGenerator { // start class
  public static void main(String[] args) { // start main method
    
int randomNumber= (int) (Math.random() * 52 + 1); // generate random numbers 1 to 52
String suitName = ""; // declare suitName to be a String
String cardIdentity = ""; // declare cardIdentity to be a String

// below, assign suit names using if statements     
if(randomNumber <= 13) { // start if diamonds
  suitName= "Diamonds";
  } // end if diamonds
else if(randomNumber <= 26) { // start if clubs
  suitName= "Clubs";
  } // end if clubs 
else if (randomNumber <=39) { // start if hearts
  suitName= "Hearts";
  } // end if hearts
else if (randomNumber <=52) { // start if spades
  suitName= "Spades";
  } // end if spades    
    
 // below, use switch statement to assign card identity
 int toggle = randomNumber % 13; // declare temporary variable toggle to be an int. Assign toggle to equal randomNumber mod 13
    switch(toggle) { // start switch 
      case 1:
        cardIdentity = "Ace"; // if result is 1 the cardIdentity is ace
        break; // if case is true, skip next cases and go to print statement 
      case 2:
        cardIdentity = "2"; // if result is 2 the cardIdentity is 2
        break; // if case is true, skip next cases and go to print statement
      case 3:
        cardIdentity = "3"; // if result is 3 the cardIdentity is 3
        break; // if case is true, skip next cases and go to print statement
      case 4:
        cardIdentity = "4"; // if result is 4 the cardIdentity is 4
        break; // if case is true, skip next cases and go to print statement
      case 5:
        cardIdentity = "5"; // if result is 5 the cardIdentity is 5
        break; // if case is true, skip next cases and go to print statement
      case 6:
        cardIdentity = "6"; // if result is 6 the cardIdentity is 6
        break; // if case is true, skip next cases and go to print statement
      case 7:
        cardIdentity = "7"; // if result is 7 the cardIdentity is 7
        break; // if case is true, skip next cases and go to print statement
      case 8:
        cardIdentity = "8"; // if result is 8 the cardIdentity is 8
        break; // if case is true, skip next cases and go to print statement
      case 9:
        cardIdentity = "9"; // if result is 9 the cardIdentity is 9
        break; // if case is true, skip next cases and go to print statement
      case 10:
        cardIdentity = "10"; // if result is 10 the cardIdentity is 10
        break; // if case is true, skip next cases and go to print statement
      case 11:
        cardIdentity = "Jack"; // if result is 11 the cardIdentity is Jack
        break; // if case is true, skip next cases and go to print statement
      case 12:
        cardIdentity = "Queen"; // if result is 12 the cardIdentity is Queen
        break; // if case is true, skip next cases and go to print statement
      case 0:
        cardIdentity = "King"; // if result is 0 the cardIdentity is King
        break; // go to print statement
      
    } // end switch
    
// below print the randomly selected card
    System.out.println("The random number Picked: " + randomNumber + " means that You picked the " + cardIdentity + " of " + suitName);
    
  } // end main method
  
} // end class 