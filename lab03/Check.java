// Tyler A. Hugo
// CSE2 lab03 2/9/18
// this program calculates how much each person in a group owes for dinner, including the tip  
import java.util.Scanner; // import scanner class
public class Check { // start class
  public static void main(String[] args) { // start main method
   Scanner myScanner = new Scanner ( System.in ) ; // declare instance of scanner
   System.out.print("Enter the original cost of the check in the form xx.xx: ") ; // prompt user for original cost of check
   double checkCost = myScanner.nextDouble() ; // declare checkCost to be a double
   System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ") ;
    // above prompt user for tip percentage and accept the input
   double tipPercent = myScanner.nextDouble() ; // declare tipPercent to be a double
   tipPercent /= 100; // convert the percentage into a decimal value
   System.out.print("Enter the number of people who went out to dinner: ") ; 
    // above prompt the user for the number of people that went to dinner
   int numPeople = myScanner.nextInt() ; // declare numPeople to be an integer
   // output
   double totalCost; // declare totalCost to be a double
   double costPerPerson; // declare costPerPerson to be a double 
   int dollars, // whole dollar ammount of cost as an integer
        dimes, // for storing digits to the right of the decimal point for the cost$
        pennies; // for storing digits to the right of the decimal point for the cost$
    
   totalCost = checkCost * (1 + tipPercent); // assign totalCost to = checkCost plus tipPercent as a dollar amount
costPerPerson = totalCost / numPeople; // assign costPerPerson to = totalCost divived by numPeople
dollars = (int) costPerPerson; //get the whole amount, dropping decimal fraction
dimes = (int) (costPerPerson * 10) % 10; // get dimes amount
pennies = (int) (costPerPerson * 100) % 10; // get pennies amount 
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // print amount each person owes
    

   
  } // end of main method
} // end of class 