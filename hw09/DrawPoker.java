// Tyler Hugo
// CSE2 hw09 4/16/18
// this program picks a hand of 5 cards for two players out of a shuffled deck of cards
// the program checks to see if each player has a pair, three of a kind, flush, or full house
// then the program picks a winner using either that information or whoever has the highest card 
import java.util.Random; 
public class DrawPoker {
  public static void main(String[] args){

int[]  deck = new int[52]; // create array for a deck of 52 cards 0-51
    for(int i = 0; i < deck.length; i++){
      deck[i] = i;
    } // end for 

int[] shuffleDeck = shuffle(deck); // create array for shuffled deck 
    
int[] player1 = new int[5]; // deal 5 card hand to player 1 
   int index = 0; 
	 System.out.println("Player One Card Numbers: ");
   for(int j = 0; j < player1.length; j++){
      player1[j] = shuffleDeck[index]; 
      index += 2; 
     System.out.print(player1[j]%13 +" , "); // print players card numbers 
   } // end or j 
	 System.out.println(); 
   
	//below booleans show true if player's hand has pair, three, flush, or full house 
   Boolean hasPair1 = pair(player1); 
     System.out.println("player 1 has pair: "+hasPair1); 
   Boolean hasThree1 = three(player1);
     System.out.println("player 1 has three: "+hasThree1);
	 Boolean hasFlush1 = flush(player1);
		 System.out.println("player 1 has flush: "+hasFlush1);
	 Boolean hasFullHouse1 = fullHouse(player1);
		 System.out.println("player 1 has full house: "+hasFullHouse1);
		
// Scoring Player 1:
	int player1Score = 0; 
	if(hasPair1 == true){
		player1Score += 1;
	}
	else if(hasThree1 == true){
		player1Score += 2;
	}
	else if(hasFlush1 == true){
		player1Score += 3;
	}
  else if(hasFullHouse1 == true){
		player1Score += 4;
	}
		System.out.println("Player 1 initial score: "+player1Score); // print initial score 
		System.out.println();
 
int[] player2 = new int[5]; // deal 5 card hand to player 2
   int index2 = 1; 
	System.out.println("Player Two Card Numbers: ");
   for(int i = 0; i < player2.length; i++){
      player2[i] = shuffleDeck[index2]; 
      index2 += 2; 
     System.out.print(player2[i]%13+" , "); // print players card numbers 
   } // end or j 
	 System.out.println(); 
   
 //below booleans show true if player's hand has pair, three, flush, or full house  
    Boolean hasPair2 = pair(player2); 
     System.out.println("Player 2 has pair: "+hasPair2); 
    Boolean hasThree2 = three(player2);
     System.out.println("player 2 has three: "+hasThree2);
	  Boolean hasFlush2 = flush(player2);
		 System.out.println("player 2 has flush: "+hasFlush2);
		Boolean hasFullHouse2 = fullHouse(player2);
		 System.out.println("player 2 has full house: "+hasFullHouse2);
		
//Scoring Player 2: 
int player2Score = 0; 
	if(hasPair2 == true){
		player2Score += 1;
	}
	else if(hasThree2 == true){
		player2Score += 2;
	}
	else if(hasFlush2 == true){
		player2Score += 3;
	}
  else if(hasFullHouse2 == true){
		player2Score += 4;
	}
		System.out.println("Player 2 initial score: "+player2Score); // player 2 initial score 
		
// if its a tie, use method 'score' to find highest card in each hand
	if(player1Score == player2Score){
		player1Score = score(player1); // replace score with highest card 
		player2Score = score(player2); // replace score with highest card 
		System.out.println("Tie--> Player 1 score: "+player1Score+" , Player 2 score: "+player2Score);
	}// end if 
		
// determine winner
	if(player1Score > player2Score){
		System.out.println("***Player 1 is the winner***");
	}
	else{
		System.out.println("***Player 2 is the winner***");
	}
    
  } // end main Method 
  
public static void print(int[] input) { // this method is used throughout to print arrays 
  String out="{";
	for(int j=0;j<input.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=input[j];
	}
	out+="} ";
	System.out.println(out);
} // end metod print 
  
// the method below shuffles the deck of cards   
public static int[] shuffle(int[] list){
  Random randomNum = new Random(); // random number generator
  int index, temp; 
  for(int i = list.length - 1; i > 0; i--){ 
   index = randomNum.nextInt(i + 1);  
   temp = list[index];
   list[index] = list[i];
   list[i] = temp;
  } // end for 
  return list; 
}// end method shuffle  
  
public static Boolean pair(int[] list){ // this method tests if there is a pair in the players hand
  int key; 
  for(int i = 0; i < list.length; i++){
     key = list[i] % 13; 
    for(int j = 0; j < list.length; j++){ 
     if(key == list[j] % 13 && i != j){
       return true; // if there is a pair 
     } // end if finds key
    } // end for j 
  } // end for i  
  return false; // if there is not a pair 
} // end method pair 

public static Boolean three(int[] list){ // this method tests if there are 3 of a kind 
  int key; 
  for(int i = 0; i < list.length; i++){
     key = list[i] % 13; 
    for(int j = 0; j < list.length; j++){
      for(int k = 0; k < list.length; k++){ 
        if(key == list[j] % 13 && key == list[k] % 13 && i != j && i != k && j != k){
          return true; // if there are three of a kind 
        } // end if finds key
      } // end if k   
    } // end for j 
  } // end for i  
  return false; // if not 
} // end method three 
  
public static Boolean flush(int[] list){ // tests if there all 5 cards are of the same suit  
  int[] suit = new int[list.length];
  for(int i = 0; i < list.length; i++){
    suit[i] = (int) (list[i] / 13); 
  } // end for i 
	System.out.println("suits: "); // prints the suit of each card in the hand for convienence 
	print(suit); 
	if(suit[0] == suit[1] && suit[0] == suit[2] && suit[0] == suit[3] && suit[0] == suit[4]){
		return true; // if there is a flush 
	} // end if 
	else{
		return false; // if not 
	} // end else 
} // end method flush 
	
public static Boolean fullHouse(int[] list){ // tests if there is a full house 
	Boolean a = pair(list); // two of a kind 
	Boolean b = three(list); // three of a kind 
	if(a == true && b == true){
		return true; // if there is a fullhouse 
	}
	else{
		return false; // if not  
	}
} // end method full house 
	
public static int score(int[] list){ // this method finds the highest card in the hand 
	int highCard = list[0] % 13; 
	for(int i = 0; i < list.length; i++){
		if(list[i] % 13 > highCard){
			highCard = list[i] % 13; 
		}// end if  	
	}// end for
	return highCard; // returns value of the highest card 
} // end method score 
	
} // end class 