//Tyler Hugo
//Feb 02, 2018
//cse2 
//this program measures speed, distance, and rotations of two trips
public class Cyclometer {
		// main method
	public static void main(String[] args ) {
// our input data
		int secsTrip1=480; // declare secsTrip1 to be an integer variable
		int secsTrip2=3220; // declare secsTrip2 to be an integer variable
		int countsTrip1=1561; // declare countsTrip1 to be an integer variable
		int countsTrip2=9037; // declare countsTrip2 to be an integer variable
// our intermediate variables and output data
		double wheelDiameter=27.0, // declare wheelDiameter to be a double
		PI=3.14159, // declare PI to be a double
		feetPerMile=5280, // declare feetPerMile to be a double
		inchesPerFoot=12, // declare inchesPerFoot to be a double
		secondsPerMinute=60; // declare secondsPerMinute to be a double
		double distanceTrip1, distanceTrip2, totalDistance; // declare distanceTrip1, distanceTrip2, and totalDistance as doubles
// print numbers stored in variables for seconds and countsTrip2
		System.out.println("Trip 1 took "+
				(secsTrip1/secondsPerMinute) +" minutes and had "+
					countsTrip1+" counts. ");
		System.out.println("Trip 2 took "+
				(secsTrip2/secondsPerMinute) +" minutes and had "+
					countsTrip2+" counts.");
		// run the calculations; store the values
distanceTrip1=countsTrip1 * wheelDiameter * PI;
// above gives distance in inches
// (for each count, a rotation of the wheel travels the diameter in inches times PI)
distanceTrip1/=inchesPerFoot * feetPerMile; // gives distance in miles
distanceTrip2=countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
totalDistance=distanceTrip1 + distanceTrip2; // gives combined distance of trip1 and trip2
// Print the output data.
		System.out.println("Trip 1 was "+distanceTrip1+" miles");
		System.out.println("Trip 2 was "+distanceTrip2+" miles");
		System.out.println("The total distance was "+totalDistance+" miles");
		
		
	} //end of main method
} //end of class


