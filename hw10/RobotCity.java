//tyler hugo
//hw 10

import java.util.Random;
public class RobotCity{
  public static void main(String[] args){
int westEast, southNorth, numRobots;
Random randomNum = new Random();
// generate west/east 10-15
do{
  westEast = randomNum.nextInt(16);
} while(westEast < 10);
System.out.println("W/E width: "+ westEast);
// generate south/north 10-15
do{
  southNorth = randomNum.nextInt(16);
} while(southNorth < 10);
System.out.println("S/N height: "+ southNorth);
// make city array
int[][] cityArray = new int[westEast][southNorth];
cityArray = buildCity(westEast,southNorth); // build city
System.out.println("City before invasion: ");
display(cityArray);
// generate number of robots invading
do{
  numRobots = randomNum.nextInt(westEast * southNorth);
} while(numRobots < 1); // at least one robot invades
invade(cityArray, numRobots); // invade city
System.out.println("City after invasion: ");
display(cityArray);
// update city
for(int i = 0; i < 5; i++){
int count = i + 1;
update(cityArray);
System.out.println("City after " + count + " updates: ");
display(cityArray);
} // end for

  } // end main method

// method to build city
public static int[][] buildCity(int a, int b){
  int population;
  Random randomNum = new Random();
  // make array
  int[][] array = new int[a][b];
  for(int i = 0; i < array.length; i++){ // row
    for(int j = 0; j < array[0].length; j++){ // coll
      // generate random population
      do{
        population = randomNum.nextInt(1000);
      } while(population < 100);
      array[i][j] = population;
    } // end for j
  } // end for i
  return array;
} // end method build city

// method to print spreadsheet
public static void display(int[][] array){
  for(int j = 0; j < array[0].length; j++){ // col
    for(int i = 0;i < array.length;i++){ // row
      System.out.printf("%6s", array[i][j] + " ");
    } //end for i
    System.out.println();
  } // end for j
}// end method display

// method to invade city
public static void invade(int[][] array, int k){
  Random randomNum = new Random();
  int a, b;
  for(int i = 0; i < k; i++){
    do{
    a = randomNum.nextInt(array.length);
    b = randomNum.nextInt(array[0].length);
  } while(array[a][b] < 0);
    array[a][b] = -(array[a][b]); // invaded block is -
  } // end for
} // end method invade

// method to update robots
public static void update(int[][] array){
  // copy array
  int[][] copy = new int[array.length][array[0].length];
   for(int i = 0; i < array.length; i++){
     for(int j = 0; j < array[0].length; j++){
       copy[i][j] = array[i][j];
     } // end for j
   } // end for i
// make array all positive
  for(int i = 0; i < array.length; i++){ // row
    for(int j = 0; j < array[0].length; j++){ // coll
    if(copy[i][j] < 0){
      array[i][j] = copy[i][j] * -1;
    } // end if < 0
    else{
      array[i][j] = copy[i][j];
    }
} // end for j
} // end for i
// move robot over
for(int i = 0; i < array.length; i++){ // row
  for(int j = 0; j < array[0].length; j++){ // coll
  if(i < array.length - 2){
  if(copy[i][j] < 0){
    array[i+1][j] = array[i+1][j] * -1;
  } // end if i +1
} // end if
} // end for j
} // end for i
} // end method update

} // end class
