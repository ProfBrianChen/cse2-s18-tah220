// Tyler A. Hugo
// CSE2 hw03
// This program takes afffected area in acres and rain fall in inches and outputs quantity of rain in cubic miles  

import java.util.Scanner; // import scanner
public class Convert { // start class
  public static void main(String[] args) { // start main method
    
Scanner myScanner = new Scanner ( System.in ) ; // declare instance of scanner
System.out.print("Enter the affected area in acres in the form xxxxx.xx: "); // prompt user for affected area in acres
    double affectedArea = myScanner.nextDouble(); // declare affectedArea to be a double and print nextDouble
    affectedArea /= 640; // convert acres to square miles
System.out.print("Enter the rainfall in the affected area in inches in the form xx: "); // prompt user for rainfall in inches
    double rainFall = myScanner.nextDouble(); // declare rainFall to be a double and print next double
    rainFall /= 63360; // convert inches to miles
double cubicMiles; // declare cubicMiles to be a double
    cubicMiles = affectedArea * rainFall; // assign cubicMiles to be the product of affectedArea and rainFall
System.out.println("Quantity of rain: " + cubicMiles + " cubic miles"); // print quantity of rain in cubic miles


  } // end main method
  
} // end class