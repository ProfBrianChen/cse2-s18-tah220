// Tyler A. Hugo
// CSE2 hw03
// This program takes the input height and length of a pyramid from the user and calculates and outputs the volume. 

import java.util.Scanner; // import scanner
public class Pyramid { // start class
  public static void main(String[] args) { // start main method

Scanner myScanner = new Scanner ( System.in ) ; // declare instance of scanner 
System.out.print("The square side of the pyramid is (input length) : "); // prompt user to input pyramid length
    double length = myScanner.nextDouble(); // declare length to be a double and assign length to be next double typed
System.out.print("The height of the pyramid is (input height) : "); // prompt user to input pyramid height
    double height = myScanner.nextDouble(); // declare height to be a double and assign height to be next double typed
double volume; // declare volume to be a double
    volume = ((Math.pow(length, 2)) * (height / 3)); // calculate volume of the pyramid 
System.out.println("The volume inside the pyramid is: " + volume + "."); // print calculated volume of pyramid
  } // end main method
} // end class